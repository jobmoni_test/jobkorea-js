# JKMONI JS

이 곳은 잡코리아 내부 개발의 자바스크립트 표준화를 위한 공간입니다.

- - -
JKMONI JS는 (JOBKOREA + albamon) javascript 의 합성어로 잡코리아 사내 자바스크립트 표준화를 위한 프레임워크명입니다.
- - -

## 잡코리아 자바스크립트 개발 가이드

잡코리아 내부 자바스크립트 개발 시 필요한 기본적인 코딩 가이드를 설명합니다.

> [자바스크립트 개발 가이드](https://test) 바로가기

## JKMONI JS - API 가이드

JKMONI JS와 관련된 각종 기능들을 설명합니다.

- 잡코리아 JKMONI JS 가이드

> [JKMONI JS(PC용) API 가이드] 바로가기  
> [JKMONI JS(모바일용) API 가이드] 바로가기

- - -
참고 : 잡코리아 JKMONI JS는 현재 작업중입니다.
- - -

- 알바몬 JKMONI JS 가이드

> [JKMONI JS(PC용) API 가이드](https://jobkorea-js.bitbucket.io/albamon/pc/) 바로가기  
> [JKMONI JS(모바일용) API 가이드](https://jobkorea-js.bitbucket.io/albamon/m/) 바로가기
