﻿var AnsyncKeywordCheck = function (options) {
    this.options = $.extend(this.options, {}, options);
    this._queList = new Array();
}

AnsyncKeywordCheck.prototype.options =
{
    url: "/community/Common/ContentKeywordCheck",
    callbackComplete: undefined
}

AnsyncKeywordCheck.prototype.push = function (content, callbackReject) {
    if (callbackReject == undefined || callbackReject == null) {
        callbackReject = function (queIndex, message) {
            alert(message);
        }
    }
    this._queList.push({ content: content, callbackReject: callbackReject });
}

AnsyncKeywordCheck.prototype.Execute = function () {
    var root = this;
    this._queIndex = 0;

    var executeProcess = function () {
        var cInfo = root._queList[root._queIndex];

        var requestData = { Content: escape(cInfo.content) };

        $.ajax({
            type: "POST", url: root.options.url, dataType: "json", data: requestData, async: true,
            error: function (e) { },
            success: function (data) {

                var result;
                if (typeof data === "object") {
                    result = data;
                } else {
                    result = JSON.parse(data);
                }

                if (result.Success) {
                    if (result.Result == false) {
                        if ($.isFunction(cInfo.callbackReject)) {
                            cInfo.callbackReject(root._queIndex, result.ResultMessage);
                        }
                    } else {
                        root._queIndex++;
                        checkNextProcess();
                    }
                } else {
                    alert("요청을 실패하였습니다.");
                }
            }
        });
    }

    var checkNextProcess = function () {
        if (root._queIndex < root._queList.length) {
            executeProcess();
        } else {
            if ($.isFunction(root.options.callbackComplete)) {
                root.options.callbackComplete();
            }
        }
    }

    checkNextProcess();
}